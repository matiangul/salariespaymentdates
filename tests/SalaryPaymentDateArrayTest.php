<?php

use Assertis\SalaryPaymentDateArray;

/**
 * Class SalaryPaymentDateArrayTest
 */
class SalaryPaymentDateArrayTest extends PHPUnit_Framework_TestCase
{
    const YEAR = '2015';

    /**
     * @var Assertis\SalaryPaymentDateArray
     */
    private $object;
    /**
     * @var DateTimeZone
     */
    private $timezone;

    /**
     * Set up
     */
    public function setUp() {
        $this->timezone = new DateTimeZone('Europe/London');
        $this->object = new SalaryPaymentDateArray(self::YEAR, $this->timezone);
    }

    /**
     * @test
     */
    public function salaryPaymentDateArrayClassShouldExists() {
        $this->assertInstanceOf('Assertis\SalaryPaymentDateArray', $this->object);
    }

    /**
     * @test
     */
    public function salaryPaymentDateArrayClassShouldImplementsArrayAccess() {
        $this->assertInstanceOf('ArrayAccess', $this->object);
    }

    /**
     * @test
     */
    public function haveNoKeyForInvalidMonth() {
        $this->assertArrayNotHasKey('IvalidMonth', $this->object,
            'SalaryPaymentDateArray should not have key for invalid month');
    }

    /**
     * @test
     */
    public function haveNoValuesForInvalidMonth() {
        $this->assertNull($this->object['IvalidMonth'],
            'SalaryPaymentDateArray should not have value for invalid month');
    }

    /**
     * @test
     */
    public function haveSomeValueForValidMonthName() {
        $this->assertArrayHasKey('January', $this->object,
            'SalaryPaymentDateArray should have date for valid month');
    }

    /**
     * @test
     *
     * @expectedException \BadMethodCallException
     * @expectedExceptionMessage Cannot set salary payment dates
     */
    public function doNotAllowChangingTheArrayValues() {
        $this->object['something'] = new \DateTime('now', $this->timezone);
    }

    /**
     * @test
     *
     * @expectedException \BadMethodCallException
     * @expectedExceptionMessage Cannot unset salary payment dates
     */
    public function doNotAllowRemovingTheArrayValues() {
        unset($this->object['something']);
    }

    /**
     * @test
     */
    public function haveDateTimeValueForValidMonthName() {
        $this->assertInstanceOf('DateTime', $this->object['January'],
            'SalaryPaymentDateArray should have DateTime object for valid month');
    }

    /**
     * @test
     */
    public function returnLastWeekdayOfJanuary2015ForJanuaryKeyAnd2015YearWithTimezoneLondon() {
        $this->assertEquals(new \DateTime('30th January 2015', $this->timezone), $this->object['January'],
            'SalaryPaymentDateArray should return last weekday of January 2015');
    }

    /**
     * @test
     */
    public function returnLastWeekdayOfFebruary2015ForFebruaryKeyAnd2015YearWithTimezoneLondon() {
        $this->assertEquals(new \DateTime('27th February 2015', $this->timezone), $this->object['February'],
            'SalaryPaymentDateArray should return last weekday of February 2015');
    }

    /**
     * @test
     */
    public function returnValuesAreDateTimeRowValues() {
        $this->assertInstanceOf(Assertis\DateTimeRowValue::class, $this->object['February']);
    }

    /**
     * @test
     *
     * @dataProvider monthAndValidDatesProviderFor2015
     *
     * @param string $month month from provider
     * @param string $date  date from provider
     */
    public function returnLastWeekdayOfMonthOf2015ForMonthKeyAnd2015YearWithTimezoneLondon($month, $date) {
        $this->assertEquals(new \DateTime($date, $this->timezone), $this->object[$month],
            "SalaryPaymentDateArray should return last weekday of {$month} 2015");
    }

    /**
     * Data provider for test
     *
     * @return array
     */
    public function monthAndValidDatesProviderFor2015() {
        return [
            ['January','30th January 2015'],
            ['February','27th February 2015'],
            ['March','31th March 2015'],
            ['April','30th April 2015'],
            ['May','29th May 2015'],
            ['June','30th June 2015'],
            ['July','31th July 2015'],
            ['August','31th August 2015'],
            ['September','30th September 2015'],
            ['October','30th October 2015'],
            ['November','30th November 2015'],
            ['December','31th December 2015']
        ];
    }
}
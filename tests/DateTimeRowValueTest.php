<?php

use Assertis\DateTimeRowValue;

/**
 * Class DateTimeRowValueTest
 */
class DateTimeRowValueTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var Assertis\DateTimeRowValue
     */
    private $object;

    /**
     * Set up
     */
    public function setUp() {
        $this->object = new DateTimeRowValue('1st January 2015');
    }

    /**
     * @test
     */
    public function dateTimeRowValueClassShouldExists() {
        $this->assertInstanceOf(DateTimeRowValue::class, $this->object);
    }

    /**
     * @test
     */
    public function dateTimeRowValueClassShouldExtendsDateTime() {
        $this->assertInstanceOf(\DateTime::class, $this->object);
    }

    /**
     * @test
     */
    public function dateTimeRowValueClassShouldImplementsRowValueInterface() {
        $this->assertInstanceOf(\Assertis\RowValueInterface::class, $this->object);
    }

    /**
     * @test
     */
    public function printDateInFormatdmY() {
        $this->assertEquals('01/01/2015', $this->object->printValue());
    }
}
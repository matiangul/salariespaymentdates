<?php

use Assertis\InvalidFileHandleException;

/**
 * Class InvalidFileHandleExceptionTest
 */
class InvalidFileHandleExceptionTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var Assertis\InvalidFileHandleException
     */
    private $exception;

    /**
     * Set up
     */
    public function setUp() {
        $this->exception = new InvalidFileHandleException();
    }

    /**
     * @test
     */
    public function exceptionClassShouldExists() {
        $this->assertInstanceOf('Assertis\InvalidFileHandleException', $this->exception);
    }

    /**
     * @test
     */
    public function exceptionClassShouldBeRuntimeTypeException() {
        $this->assertInstanceOf('\RuntimeException', $this->exception);
    }
}
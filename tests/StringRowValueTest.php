<?php

use Assertis\StringRowValue;

/**
 * Class StringRowValueTest
 */
class StringRowValueTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var Assertis\StringRowValue
     */
    private $object;

    /**
     * Set up
     */
    public function setUp() {
        $this->object = new StringRowValue('ala, ma kota');
    }

    /**
     * @test
     */
    public function stringRowValueClassShouldExists() {
        $this->assertInstanceOf(StringRowValue::class, $this->object);
    }

    /**
     * @test
     */
    public function stringRowValueClassShouldImplementsRowValueInterface() {
        $this->assertInstanceOf(\Assertis\RowValueInterface::class, $this->object);
    }

    /**
     * @test
     */
    public function printValueAsIs() {
        $this->assertEquals('ala, ma kota', $this->object->printValue());
    }

    /**
     * @test
     */
    public function convertObjectToValue() {
        $this->assertEquals('ala, ma kota', $this->object);
    }
}
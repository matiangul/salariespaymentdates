<?php

use Assertis\BonusPaymentDateArray;

/**
 * Class BonusPaymentDateArrayTest
 */
class BonusPaymentDateArrayTest extends PHPUnit_Framework_TestCase
{
    const YEAR = '2015';

    /**
     * @var Assertis\BonusPaymentDateArray
     */
    private $object;
    /**
     * @var DateTimeZone
     */
    private $timezone;

    /**
     * Set up
     */
    public function setUp() {
        $this->timezone = new DateTimeZone('Europe/London');
        $this->object = new BonusPaymentDateArray(self::YEAR, $this->timezone);
    }

    /**
     * @test
     */
    public function bonusPaymentDateArrayClassShouldExists() {
        $this->assertInstanceOf('Assertis\BonusPaymentDateArray', $this->object);
    }

    /**
     * @test
     */
    public function bonusPaymentDateArrayClassShouldImplementsArrayAccess() {
        $this->assertInstanceOf('ArrayAccess', $this->object);
    }

    /**
     * @test
     */
    public function haveNoKeyForInvalidMonth() {
        $this->assertArrayNotHasKey('IvalidMonth', $this->object,
            'BonusPaymentDateArray should not have key for invalid month');
    }

    /**
     * @test
     */
    public function haveNoValuesForInvalidMonth() {
        $this->assertNull($this->object['IvalidMonth'],
            'BonusPaymentDateArray should not have value for invalid month');
    }

    /**
     * @test
     */
    public function haveSomeValueForValidMonthName() {
        $this->assertArrayHasKey('January', $this->object,
            'BonusPaymentDateArray should have date for valid month');
    }

    /**
     * @test
     *
     * @expectedException \BadMethodCallException
     * @expectedExceptionMessage Cannot set bonus payment dates
     */
    public function doNotAllowChangingTheArrayValues() {
        $this->object['something'] = new \DateTime('now', $this->timezone);
    }

    /**
     * @test
     *
     * @expectedException \BadMethodCallException
     * @expectedExceptionMessage Cannot unset bonus payment dates
     */
    public function doNotAllowRemovingTheArrayValues() {
        unset($this->object['January']);
    }

    /**
     * @test
     */
    public function haveDateTimeValueForValidMonthName() {
        $this->assertInstanceOf('DateTime', $this->object['January'],
            'BonusPaymentDateArray should have DateTime object for valid month');
    }

    /**
     * @test
     */
    public function return15thOfJanuary2015ForJanuaryKeyAnd2015YearWithTimezoneLondon() {
        $this->assertEquals(new \DateTime('15th January 2015', $this->timezone), $this->object['January'],
            'BonusPaymentDateArray should return 15th of January 2015 because it is weekday');
    }

    /**
     * @test
     */
    public function returnLastWeekdayOfFebruary2015ForFebruaryKeyAnd2015YearWithTimezoneLondon() {
        $this->assertEquals(new \DateTime('18th February 2015', $this->timezone), $this->object['February'],
            'BonusPaymentDateArray should return first Wednesday of February 2015 after 15th because 15th is a weekend');
    }

    /**
     * @test
     */
    public function returnValuesAreDateTimeRowValues() {
        $this->assertInstanceOf(Assertis\DateTimeRowValue::class, $this->object['February']);
    }

    /**
     * @test
     *
     * @dataProvider monthAndValidDatesProviderFor2015
     *
     * @param string $month month from provider
     * @param string $date  date from provider
     */
    public function return15IfItsWeekdayOrNearestWednesdayAfter15thOtherwiseWithTimezoneLondonFor2015($month, $date) {
        $this->assertEquals(new \DateTime($date, $this->timezone), $this->object[$month],
            "BonusPaymentDateArray should return 15th if its weekday or nearest wednesday after 15th otherwise for {$month} 2015");
    }

    /**
     * Data provider for test
     *
     * @return array
     */
    public function monthAndValidDatesProviderFor2015() {
        return [
            ['January','15th January 2015'],
            ['February','18th February 2015'],
            ['March','18th March 2015'],
            ['April','15th April 2015'],
            ['May','15th May 2015'],
            ['June','15th June 2015'],
            ['July','15th July 2015'],
            ['August','19th August 2015'],
            ['September','15th September 2015'],
            ['October','15th October 2015'],
            ['November','18th November 2015'],
            ['December','15th December 2015']
        ];
    }
}
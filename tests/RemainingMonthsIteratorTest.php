<?php

use Assertis\RemainingMonthsIterator;

/**
 * Class RemainingMonthsIteratorTest
 */
class RemainingMonthsIteratorTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var Assertis\RemainingMonthsIterator
     */
    private $object;
    /**
     * @var DateTimeZone
     */
    private $timezone;

    /**
     * Set up
     */
    public function setUp() {
        $this->timezone = new DateTimeZone('Europe/London');
        $this->object = new RemainingMonthsIterator(new DateTime('today', $this->timezone));
    }

    /**
     * @test
     */
    public function beItself() {
        $this->assertInstanceOf('Assertis\RemainingMonthsIterator', $this->object);
    }

    /**
     * @test
     */
    public function beTypeOfIterator() {
        $this->assertInstanceOf('\Iterator', $this->object);
    }

    /**
     * @test
     */
    public function returnAll2014YearMonthsWhenIGiveItLastDayOfJanuary() {
        $this->object = new RemainingMonthsIterator(new DateTime('last day of January 2014', $this->timezone));
        foreach ($this->object as $key => $month) {
            $last = $key;
            $lastMonth = $month;
        }
        $this->assertEquals(11, $last);
        $this->assertEquals('December', $lastMonth);
    }

    /**
     * @test
     */
    public function returnAllWithoutJanuary2015YearMonthsWhenIGiveItSomeDayOfFebruary() {
        $this->object = new RemainingMonthsIterator(new DateTime('15th February 2015', $this->timezone));
        foreach ($this->object as $key => $month) {
            $last = $key;
            $lastMonth = $month;
        }
        $this->assertEquals(10, $last);
        $this->assertEquals('December', $lastMonth);
    }

    /**
     * @test
     */
    public function returnOnly2MonthsOf2014() {
        $this->object = new RemainingMonthsIterator(new DateTime('15 November 2014', $this->timezone));
        foreach ($this->object as $key => $month) {
            $last = $key;
            $lastMonth = $month;
        }
        $this->assertEquals(1, $last);
        $this->assertEquals('December', $lastMonth);
    }
}
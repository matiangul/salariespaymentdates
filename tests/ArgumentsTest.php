<?php

use Assertis\Arguments;

/**
 * Class ArgumentsTest
 */
class ArgumentsTest extends PHPUnit_Framework_TestCase
{
    const FILENAME = 'test.csv';

    /**
     * @var Assertis\Arguments
     */
    private $object;

    /**
     * Set up
     */
    public function setUp() {
        $this->object = new Arguments([1=>'a'], [1=>'should be here']);
    }

    /**
     * @test
     */
    public function argumentsClassShouldExists() {
        $this->assertInstanceOf('Assertis\Arguments', $this->object);
    }

    /**
     * @test
     */
    public function validateUsingSecondConstructorParameter() {
        $this->object = new Arguments([1=>'a'], [1=>'should be here']);
        $this->assertEquals('a', $this->object->get(1));
    }

    /**
     * @test
     *
     * @expectedException \InvalidArgumentException
     * @expectedExceptionMessage should be here
     */
    public function throwExceptionsWhenThereIsNoArgument() {
        $this->object = new Arguments([], [1=>'should be here']);
    }
}
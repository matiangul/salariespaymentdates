<?php

use Assertis\CsvWriter;
use org\bovigo\vfs\vfsStream,
    org\bovigo\vfs\vfsStreamDirectory;

/**
 * Class CsvWriterTest
 */
class CsvWriterTest extends PHPUnit_Framework_TestCase
{
    const FILENAME = 'test.csv';

    /**
     * @var Assertis\CsvWriter
     */
    private $object;
    /**
     * @var vfsStreamDirectory
     */
    private $directory;

    /**
     * Set up
     */
    public function setUp() {
        $this->directory = vfsStream::setup('dir');
        $this->object = new CsvWriter(self::FILENAME, $this->directory->url());
    }

    /**
     * @test
     */
    public function writerClassShouldExists() {
        $this->assertInstanceOf('Assertis\CsvWriter', $this->object);
    }

    /**
     * @test
     */
    public function createsAFile() {
        $this->assertTrue($this->directory->hasChild(self::FILENAME), 'file has not been created by CsvWriter');
    }

    /**
     * @test
     */
    public function writeToFile() {
        $this->object->write(['ala', 'ma', 'kota']);
        $this->assertEquals("ala,ma,kota\n", file_get_contents($this->directory->getChild(self::FILENAME)->url()));
    }

    /**
     * @test
     * @expectedException Assertis\InvalidFileHandleException
     * @expectedExceptionMessage File handle is closed
     */
    public function shouldClosesFileHandle() {
        $this->object->close();
        $this->object->write(['a', 'b']);
    }
}
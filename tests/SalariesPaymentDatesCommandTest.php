<?php

use Assertis\BonusPaymentDateArray;
use Assertis\CommandInterface;
use Assertis\CsvWriter;
use Assertis\RemainingMonthsIterator;
use Assertis\SalariesPaymentDatesCommand;
use Assertis\SalaryPaymentDateArray;
use org\bovigo\vfs\vfsStream,
    org\bovigo\vfs\vfsStreamDirectory;

/**
 * Class SalariesPaymentDatesCommandTest
 */
class SalariesPaymentDatesCommandTest extends PHPUnit_Framework_TestCase
{
    const FILENAME = 'out.csv';

    /**
     * @var Assertis\SalariesPaymentDatesCommand
     */
    private $object;
    /**
     * @var vfsStreamDirectory
     */
    private $directory;
    /**
     * @var DateTime
     */
    private $date;

    /**
     * Set up
     */
    public function setUp() {
        $this->directory = vfsStream::setup('dir');
        $this->date = new DateTime('2nd February 2015', new DateTimeZone('Europe/London'));
        $this->object = new SalariesPaymentDatesCommand(new CsvWriter(self::FILENAME, $this->directory->url()),
            new RemainingMonthsIterator($this->date),
            new SalaryPaymentDateArray($this->date->format('Y'), $this->date->getTimezone()),
            new BonusPaymentDateArray($this->date->format('Y'), $this->date->getTimezone())
        );
    }

    /**
     * @test
     */
    public function salaryPaymentDateArrayClassShouldExists() {
        $this->assertInstanceOf(SalariesPaymentDatesCommand::class, $this->object);
    }

    /**
     * @test
     */
    public function commandClassShouldImplementsCommandInterface() {
        $this->assertInstanceOf(CommandInterface::class, $this->object);
    }

    /**
     * @test
     */
    public function commandWriteCsvFileWith11MonthsFromFebruary2015ToDecember2015AndSalaryAndBonusPaymentDatesInConsecutiveColumns() {
        $this->object->run();
        $expectedOutput = <<<EOT
February,27/02/2015,18/02/2015
March,31/03/2015,18/03/2015
April,30/04/2015,15/04/2015
May,29/05/2015,15/05/2015
June,30/06/2015,15/06/2015
July,31/07/2015,15/07/2015
August,31/08/2015,19/08/2015
September,30/09/2015,15/09/2015
October,30/10/2015,15/10/2015
November,30/11/2015,18/11/2015
December,31/12/2015,15/12/2015

EOT;
        $this->assertEquals($expectedOutput, file_get_contents($this->directory->getChild(self::FILENAME)->url()));
    }
}
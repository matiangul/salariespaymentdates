# README #

### How to install ###

```
#!bash

composer install
```

### How to run tests ###

```
#!bash

php vendor/bin/phpunit tests/
```

### How to start command ###
```
#!bash

./console dates.csv
```
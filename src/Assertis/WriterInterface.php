<?php

namespace Assertis;


interface WriterInterface
{
    /**
     * @param array $row array of strings to be written in file row
     *
     * @return void
     * @throws InvalidFileHandleException
     */
    public function write(Array $row);

    /**
     * Closes file handle
     *
     * @return void
     */
    public function close();
}
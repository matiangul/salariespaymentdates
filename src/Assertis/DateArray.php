<?php

namespace Assertis;

/**
 * Class DateArray
 *
 * @package Assertis
 */
abstract class DateArray implements \ArrayAccess
{
    /**
     * @var string
     */
    protected $year;
    /**
     * @var \DateTimeZone
     */
    protected $timezone;

    /**
     * Constructor
     *
     * @param string        $year     year
     * @param \DateTimeZone $timezone timezone
     */
    public function __construct($year, \DateTimeZone $timezone)
    {
        $this->year = $year;
        $this->timezone = $timezone;
    }

    /**
     * Whether a month offset exists
     *
     * @param string $month A month to check for
     *
     * @return boolean true on success or false on failure
     */
    public function offsetExists($month)
    {
        $parsedDate = date_parse($month);
        return false !== $parsedDate['month'];
    }
}
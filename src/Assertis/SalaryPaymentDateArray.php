<?php

namespace Assertis;

/**
 * Class SalaryPaymentDateArray
 *
 * @package Assertis
 */
class SalaryPaymentDateArray extends DateArray
{
    /**
     * Month offset to retrieve
     *
     * @param string $month A month to get date
     *
     * @return DateTimeRowValue
     */
    public function offsetGet($month)
    {
        if (true === $this->offsetExists($month)) {
            $nextMonth = (new \DateTime("first day of $month {$this->year}", $this->timezone))->modify('first day of next month');
            $month = $nextMonth->format('F');
            $year = $nextMonth->format('Y');

            return new DateTimeRowValue("last weekday first day of {$month} {$year}", $this->timezone);
        }

        return null;
    }

    /**
     * Offset to set
     *
     * @param mixed $month The month to assign the value to
     * @param mixed $value The value to set
     *
     * @return void
     *
     * @throws \BadMethodCallException
     */
    public function offsetSet($month, $value)
    {
        throw new \BadMethodCallException('Cannot set salary payment dates');
    }

    /**
     * Offset to unset
     *
     * @param mixed $month The month to unset
     *
     * @return void
     *
     * @throws \BadMethodCallException
     */
    public function offsetUnset($month)
    {
        throw new \BadMethodCallException('Cannot unset salary payment dates');
    }
}
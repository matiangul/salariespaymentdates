<?php

namespace Assertis;

class SalariesPaymentDatesCommand implements CommandInterface
{
    /**
     * @var  WriterInterface
     */
    private $writer;
    /**
     * @var \Iterator
     */
    private $indexColumn;
    /**
     * @var array
     */
    private $remainingColumns;

    /**
     * Command constructor
     *
     * @param WriterInterface $writer              file writer
     * @param \Iterator       $indexColumn         index column by which values from remaining columns are pulled
     * @param \ArrayAccess    ...$remainingColumns remaining columns
     */
    public function __construct(WriterInterface $writer, \Iterator $indexColumn, \ArrayAccess ...$remainingColumns) {
        $this->writer = $writer;
        $this->indexColumn = $indexColumn;
        $this->remainingColumns = $remainingColumns;
    }

    /**
     * Write columns values to file
     *
     * @return void
     */
    public function run() {
        foreach ($this->indexColumn as $index) {
            $this->writeColumn($index);
        }
    }

    /**
     * Write a row of values from columns to a file
     *
     * @param StringRowValue $index index
     *
     * @return void
     */
    private function writeColumn($index)
    {
        $row = [$index->printValue()];
        foreach ($this->remainingColumns as $column) {
            $row[] = $column[$index]->printValue();
        }
        $this->writer->write($row);
    }
}
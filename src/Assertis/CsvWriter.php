<?php

namespace Assertis;

/**
 * Class CsvWriter
 *
 * @package Assertis
 */
class CsvWriter implements WriterInterface
{
    /**
     * @var string
     */
    private $filename;
    /**
     * @var string
     */
    private $directory;
    /**
     * @var Resource
     */
    private $fileHandle = null;

    /**
     * Constructor open file to write
     *
     * @param string $filename  file name to write to
     * @param string $directory dir name where to put file
     */
    public function __construct($filename, $directory) {
        $this->filename = $filename;
        $this->directory = $directory;
        $this->fileHandle = fopen($this->directory . DIRECTORY_SEPARATOR . $this->filename, 'w');
    }

    /**
     * Destructor close file handle
     */
    public function __destruct() {
        $this->close();
    }

    /**
     * Write array of strings to file
     *
     * @param array $row array of strings to be written in csf file
     *
     * @return void
     *
     * @throws InvalidFileHandleException
     */
    public function write(Array $row)
    {
        if (null !== $this->fileHandle) {
            fputcsv($this->fileHandle, $row);
        } else {
            throw new InvalidFileHandleException('File handle is closed');
        }
    }

    /**
     * Closes file handle
     *
     * @return void
     */
    public function close()
    {
        if (null !== $this->fileHandle) {
            fclose($this->fileHandle);
        }
        $this->fileHandle = null;
    }
}
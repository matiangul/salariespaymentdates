<?php

namespace Assertis;

class StringRowValue implements RowValueInterface
{
    /**
     * @var string
     */
    private $value;

    /**
     * Constructor
     *
     * @param string $value value
     */
    public function __construct($value) {
        $this->value = $value;
    }

    /**
     * Convert object to string
     *
     * @return string
     */
    public function __toString() {
        return $this->value;
    }

    /**
     * Return string formated date
     *
     * @return string
     */
    public function printValue() {
        return $this->__toString();
    }
}
<?php

namespace Assertis;

/**
 * Interface CommandInterface
 *
 * @package Assertis
 */
interface CommandInterface
{
    /**
     * Run command
     *
     * @return void
     */
    public function run();
}
<?php

namespace Assertis;

/**
 * Class BonusPaymentDateArray
 *
 * @package Assertis
 */
class BonusPaymentDateArray extends DateArray
{
    /**
     * @var Array
     */
    private static $weekendDays = [6,7];

    /**
     * Month offset to retrieve
     *
     * @param string $month A month to get date
     *
     * @return DateTimeRowValue
     */
    public function offsetGet($month)
    {
        if (true === $this->offsetExists($month)) {
            $bonusDay = new DateTimeRowValue("15th $month {$this->year}", $this->timezone);
            if (!in_array($bonusDay->format('N'), self::$weekendDays)) {
                return $bonusDay;
            } else {
                return new DateTimeRowValue("Wednesday 15th $month {$this->year}", $this->timezone);
            }
        }

        return null;
    }

    /**
     * Offset to set
     *
     * @param mixed $month The month to assign the value to
     * @param mixed $value The value to set
     *
     * @return void
     *
     * @throws \BadMethodCallException
     */
    public function offsetSet($month, $value)
    {
        throw new \BadMethodCallException('Cannot set bonus payment dates');
    }

    /**
     * Offset to unset
     *
     * @param mixed $month The month to unset
     *
     * @return void
     *
     * @throws \BadMethodCallException
     */
    public function offsetUnset($month)
    {
        throw new \BadMethodCallException('Cannot unset bonus payment dates');
    }
}
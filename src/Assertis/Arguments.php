<?php

namespace Assertis;

/**
 * Class Arguments
 *
 * @package Assertis
 */
class Arguments
{
    /**
     * @var array
     */
    private $arguments;
    /**
     * @var array
     */
    private $messages;

    /**
     * Constructor which validates args
     *
     * @param array $arguments cli args
     * @param array $messages  messages to missing args
     */
    public function __construct(Array $arguments, Array $messages) {
        $this->arguments = $arguments;
        $this->messages = $messages;
        foreach ($this->messages as $key => $message) {
            if (!isset($this->arguments[$key])) {
                throw new \InvalidArgumentException($message);
            }
        }
    }

    /**
     * Return argument
     *
     * @param $number
     *
     * @return mixed|null
     */
    public function get($number) {
        if (isset($this->arguments[$number])) {
            return $this->arguments[$number];
        }

        return null;
    }
}
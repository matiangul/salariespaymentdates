<?php

namespace Assertis;

class RemainingMonthsIterator implements \Iterator
{
    /**
     * @var int
     */
    private $position = 0;
    /**
     * @var array
     */
    private $months = [];

    /**
     * Constructor fill table of months
     *
     * @param \DateTime $startDate
     */
    public function __construct(\DateTime $startDate) {
        $this->position = 0;
        $year = $startDate->format('Y');
        $month = $startDate->format('F');
        $date = new \DateTime("first day of $month $year", $startDate->getTimezone());
        while ($year === $date->format('Y')) {
            $this->months[] = $date->format('F');
            $date->modify('first day of next month');
        }
    }

    /**
     * Rewind the Iterator to the first element
     *
     * @return void Any returned value is ignored.
     */
    function rewind() {
        $this->position = 0;
    }

    /**
     * Return the current element
     *
     * @return StringRowValue
     */
    function current() {
        return new StringRowValue($this->months[$this->position]);
    }

    /**
     * Return the key of the current element
     *
     * @return int
     */
    function key() {
        return $this->position;
    }

    /**
     * Move forward to next element
     *
     * @return void Any returned value is ignored.
     */
    function next() {
        ++$this->position;
    }

    /**
     * Checks if current position is valid
     *
     * @return boolean The return value will be casted to boolean and then evaluated.
     * Returns true on success or false on failure.
     */
    function valid() {
        return isset($this->months[$this->position]);
    }
}
<?php

namespace Assertis;

interface RowValueInterface
{
    /**
     * Return string formated date
     *
     * @return string
     */
    public function printValue();
}
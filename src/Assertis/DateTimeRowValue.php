<?php

namespace Assertis;

/**
 * Class DateTimeRowValue
 *
 * @package Assertis
 */
class DateTimeRowValue extends \DateTime implements RowValueInterface
{
    /**
     * Return string formated date
     *
     * @return string
     */
    public function printValue() {
        return $this->format('d/m/Y');
    }
}